#include <stdio.h>
#include <windows.h>
#include <shellapi.h>

#define KEY_CTRL_LEFT_ARROW 0x25
#define KEY_CTRL_RIGHT_ARROW 0x27
#define KEY_CTRL_UP_ARROW 0x26
#define KEY_CTRL_DOWN_ARROW 0x28
#define KEY_CTRL_ALT_ENTER 0x0D
#define TRAY_ICON_ID 1001
#define WM_TRAY_ICON (WM_USER + 1)

HWND hwnd;
HMENU hMenu;

void resizeAndMoveWindow(HWND hwnd, int x, int y, int width, int height)
{
    SetWindowPos(hwnd, NULL, x, y, width, height, SWP_NOZORDER);
}

void handleKeyPress()
{
    HWND taskbarHandle = FindWindow("Shell_TrayWnd", NULL);
    RECT taskbarRect;
    int taskbarWidth = taskbarRect.right - taskbarRect.left;
    int taskbarHeight = taskbarRect.bottom - taskbarRect.top;

    printf("Taskbar Location: (%d, %d)\n", taskbarRect.left, taskbarRect.top);
    printf("Taskbar Size: %d x %d\n", taskbarWidth, taskbarHeight);

    if (taskbarHandle && GetWindowRect(taskbarHandle, &taskbarRect))
    {
        if (GetAsyncKeyState(VK_CONTROL) & 0x8000)
        {
            if (GetAsyncKeyState(VK_LEFT) & 0x8000)
            {
                HWND hwnd = GetForegroundWindow();
                HMONITOR hMonitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);
                MONITORINFO monitorInfo;
                monitorInfo.cbSize = sizeof(MONITORINFO);
                GetMonitorInfo(hMonitor, &monitorInfo);
                int screenWidth = monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left;
                int screenHeight = monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top;
                resizeAndMoveWindow(hwnd, monitorInfo.rcMonitor.left, monitorInfo.rcMonitor.top + taskbarHeight, screenWidth / 2+13, screenHeight - taskbarHeight);
            }
            else if (GetAsyncKeyState(VK_RIGHT) & 0x8000)
            {
                HWND hwnd = GetForegroundWindow();
                HMONITOR hMonitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);
                MONITORINFO monitorInfo;
                monitorInfo.cbSize = sizeof(MONITORINFO);
                GetMonitorInfo(hMonitor, &monitorInfo);
                int screenWidth = monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left;
                int screenHeight = monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top;
                resizeAndMoveWindow(hwnd, monitorInfo.rcMonitor.left + screenWidth / 2, monitorInfo.rcMonitor.top + taskbarHeight, screenWidth / 2, screenHeight - taskbarHeight);
            }
            else if (GetAsyncKeyState(VK_UP) & 0x8000)
            {
                HWND hwnd = GetForegroundWindow();
                HMONITOR hMonitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);
                MONITORINFO monitorInfo;
                monitorInfo.cbSize = sizeof(MONITORINFO);
                GetMonitorInfo(hMonitor, &monitorInfo);
                int screenWidth = monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left;
                int screenHeight = monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top;
                resizeAndMoveWindow(hwnd, monitorInfo.rcMonitor.left, monitorInfo.rcMonitor.top + taskbarHeight, screenWidth, screenHeight / 2 - taskbarHeight+13);
            }
            else if (GetAsyncKeyState(VK_DOWN) & 0x8000)
            {
                HWND hwnd = GetForegroundWindow();
                HMONITOR hMonitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);
                MONITORINFO monitorInfo;
                monitorInfo.cbSize = sizeof(MONITORINFO);
                GetMonitorInfo(hMonitor, &monitorInfo);
                int screenWidth = monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left;
                int screenHeight = monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top;
                resizeAndMoveWindow(hwnd, monitorInfo.rcMonitor.left, monitorInfo.rcMonitor.top + screenHeight / 2, screenWidth, screenHeight / 2);
            }
            else if (GetAsyncKeyState(VK_MENU) & 0x8000 && GetAsyncKeyState(VK_RETURN) & 0x8000)
            {

                HWND hwnd = GetForegroundWindow();
                HMONITOR hMonitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);
                MONITORINFO monitorInfo;
                monitorInfo.cbSize = sizeof(MONITORINFO);
                GetMonitorInfo(hMonitor, &monitorInfo);
                int screenWidth = monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left;
                int screenHeight = monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top;
                resizeAndMoveWindow(hwnd, monitorInfo.rcMonitor.left, monitorInfo.rcMonitor.top + taskbarHeight, screenWidth, screenHeight - taskbarHeight);
            }
        }
    }
    else
    {
        printf("Taskbar not found.\n");
    }
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    POINT cursorPos;

    switch (uMsg)
    {
    case WM_CREATE:
        hMenu = CreatePopupMenu();
        AppendMenu(hMenu, MF_STRING, 1, "Exit");
        break;
    case WM_TRAY_ICON:
        switch (lParam)
        {
        case WM_RBUTTONDOWN:
            GetCursorPos(&cursorPos);
            SetForegroundWindow(hwnd);
            TrackPopupMenu(hMenu, TPM_RIGHTBUTTON, cursorPos.x, cursorPos.y, 0, hwnd, NULL);
            break;
        }
        break;
    case WM_COMMAND:
        switch (LOWORD(wParam))
        {
        case 1:
            DestroyWindow(hwnd);
            break;
        }
        break;
    case WM_CLOSE:
        DestroyWindow(hwnd);
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    if (nCode == HC_ACTION && (lParam & 0x80000000) == 0)
    {
        KBDLLHOOKSTRUCT *kbStruct = (KBDLLHOOKSTRUCT *)lParam;
        if (kbStruct->vkCode == KEY_CTRL_LEFT_ARROW || kbStruct->vkCode == KEY_CTRL_RIGHT_ARROW ||
            kbStruct->vkCode == KEY_CTRL_UP_ARROW || kbStruct->vkCode == KEY_CTRL_DOWN_ARROW ||
            kbStruct->vkCode == KEY_CTRL_ALT_ENTER)
        {
            handleKeyPress();
        }
    }
    return CallNextHookEx(NULL, nCode, wParam, lParam);
}

int main()
{
    HINSTANCE hInstance = GetModuleHandle(NULL);

    // Create the window class
    WNDCLASS wc = {0};
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = "MyWindowClass";
    RegisterClass(&wc);

    // Create the window
    hwnd = CreateWindowEx(0, "MyWindowClass", "Window Resizer", 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, hInstance, NULL);

    // Create the system tray icon
    NOTIFYICONDATA nid = {0};
    nid.cbSize = sizeof(NOTIFYICONDATA);
    nid.hWnd = hwnd;
    nid.uID = TRAY_ICON_ID;
    nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
    nid.uCallbackMessage = WM_TRAY_ICON;
    nid.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    strcpy(nid.szTip, "Window Resizer");
    Shell_NotifyIcon(NIM_ADD, &nid);

    // Set the keyboard hook
    HHOOK hook = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardProc, hInstance, 0);
    if (hook == NULL)
    {
        printf("Failed to set keyboard hook.\n");
        return 1;
    }

    // Message loop
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    // Clean up
    UnhookWindowsHookEx(hook);
    Shell_NotifyIcon(NIM_DELETE, &nid);
    return 0;
}